# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  num_chosen = rand(1..100)
  puts "Guess a number!"
  num_guess = 0
  guess_counter=0
  guess_list=[]
  until num_guess == num_chosen
    puts "What number am I thinking of?"
    num_guess = gets.chomp.to_i
    if valid_guess?(num_guess)
      guess_list << num_guess
      puts "You geussed #{num_guess}"
      help_user(num_guess, num_chosen)
      guess_counter+=1
    else
      num_guess=0
    end
  end
  puts "Good job! The number is #{num_chosen}. It took you #{guess_counter} tries!"
end

def valid_guess?(num)
  num.between?(0,100)
end

def help_user(num_guess, num_chosen)
  if num_guess > num_chosen
    puts "Your guess is too high!"
  else
    puts "Your guess is too low!"
  end
end
